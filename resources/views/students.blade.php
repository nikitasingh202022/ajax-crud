<!DOCTYPE html>
<html lang="en">

<head>
    <title>Students-list</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css" rel="stylesheet">
</head>

<body>

    <div class="container mt-3">
        <h2>Students-list</h2>
        <button type="button" class="btn btn-info my-4" id="addStudentButton" style="float:right;">Add</button>
        <table class="table table-striped data-table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Student's Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Address</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="addModal" area-hidden="true">
        <div class="modal-dialog" >
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalHeading"></h5>
                </div>
                <div class="modal-body">
                    <form id="addStudentForm" name="addStudentForm" class="form-horizontal">
                        <div class="form-group mt-2">
                            Name:<br>
                            <input type="text" name="name" id="name" class="form-control mt-2" placeholder="Enter Name" value="" required />
                        </div>
                        <div class="form-group mt-2">
                            Email:<br>
                            <input type="email" name="email" id="email" class="form-control mt-2" placeholder="Enter email" value="" required />
                        </div>
                        <div class="form-group mt-2">
                            Mobile:<br>
                            <input type="text" name="mobile" id="mobile" class="form-control mt-2" placeholder="Enter mobile" value="" required />
                        </div>
                        <div class="form-group mt-2">
                            Address:<br>
                            <textarea name="address" id="address" class="form-control mt-2 mb-4" placeholder="Enter address" required ></textarea>
                        </div>
                        <input type="hidden" name="student_id" id="student_id" />
                        <meta name="csrf-token" content="{{ csrf_token() }}">
                        <button type="submit" class="mt-2 btn btn-primary" id="createBtn" value="create">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

<script src="https://code.jquery.com/jquery-3.6.3.min.js" integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
</body>
<script type="text/javascript">

    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var table = $('.data-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('students.index') }}",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'mobile', name: 'mobile'},
                {data: 'address', name: 'address'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });

        $('#addStudentButton').click(function () {
            $('#student_id').val('');
            $('#addStudentForm').trigger("reset");
            $('#modalHeading').html("Add Student");
            $('#addModal').modal('show');
        });

        $('#createBtn').click(function (e) {
            e.preventDefault();
            $.ajax({
                data: $('#addStudentForm').serialize(),
                url: "{{ route('students.store') }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    $('#addStudentForm').trigger("reset");
                    $('#addModal').modal('hide');
                    table.draw();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        $('body').on('click', '.deleteStudent', function(){
            var student_id = $(this).data('id');
            confirm("Are you sure, want to delete?");
            $.ajax({
                url: "{{ route('students.store') }}"+'/'+student_id,
                type: "DELETE",
                success: function (data) {
                    table.draw();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            })
        });

        $('body').on('click', '.editStudent', function(){
            var student_id = $(this).data('id');
            $.get("{{route('students.index')}}"+'/'+student_id+"/edit", function(data){
                $('#modalHeading').html("Edit Student");
                $('#addModal').modal('show');
                $('#student_id').val(data[0].id);
                $('#name').val(data[0].name);
                $('#email').val(data[0].email);
                $('#mobile').val(data[0].mobile);
                $('#address').val(data[0].address);
            });
        });

    });
</script>
</html>
