<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\StudentModel;
use DataTables;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $students= StudentModel::get();
        // dd($request);
        if($request->ajax()){
            $allData= DataTables::of($students)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $btn= '<a href="javascript:void(0);" data-toggle="tooltip" data-id="'.$row->id.'" data-original-title="edit" class="edit btn btn-primary btn-sm editStudent">Edit</a>';
                $btn .= ' <a href="javascript:void(0);" data-toggle="tooltip" data-id="'.$row->id.'" data-original-title="delete" class="edit btn btn-danger btn-sm deleteStudent">Delete</a>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
            return $allData;
        }
        return view('students', compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        StudentModel::updateOrCreate(['id'=>$request->student_id],
        ['name'=>$request->name,
        'email'=>$request->email,
        'mobile'=>$request->mobile,
        'address'=>$request->address
        ]);
        return response()->json(['success'=> 'Student Added Successfully!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $students= StudentModel::find($id);
        return response()->json([$students]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        StudentModel::find($id)->delete();
        return response()->json(['success'=>'Student deleted successfully.']);
    }
}
